<?php require_once "./code.php"; ?>

<html><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Divisible of Five</h1>
    <?php forLoop(); ?>

    <h1>Array Manipulation</h1>
    <?php array_push($students, 'Sam'); ?>
    <p><?php print_r($students); ?></p>
    <p><?php echo count($students); ?></p>
    <?php array_push($students, 'Paul'); ?>
    <p><?php print_r($students); ?></p>
    <p><?php echo count($students); ?></p>
    <?php array_shift($students); ?>
    <p><?php print_r($students); ?></p>
    <p><?php echo count($students); ?></p>
    
</body>
</html>